#include <Average.h>
#define MINTHRESH 700

// IR sensor pins
// _ \ | / _
// 0 1 2 3 4
const int ir0 = A0;
const int ir1 = A1;
const int ir2 = A2;
const int ir3 = A3;
const int ir4 = A6;
int irInput[] = {A0,A1,A2,A3,A6};

unsigned int sensorValues[5];

void setup()
{
	Serial.begin(9600);
	// Pin Assignments
	
	//ir sensors to analog inputs, will return value of 0-1023
 	//uncessary for analogread but included for code readability 
 	Serial.println("Assigning IR sensors");
 	pinMode(ir0, INPUT);
 	pinMode(ir1, INPUT);
  	pinMode(ir2, INPUT);
  	pinMode(ir3, INPUT);
  	pinMode(ir4, INPUT);

 }

 void loop()
 {
 	readSensors();

 	if( sensorValues[2] < MINTHRESH){
 		Serial.println("Wall in front");
 	}
 	else
 		Serial.print("Left wall\t");
 		Serial.println(sensorValues[0]);
 		Serial.print("Left 45\t");
 		Serial.println(sensorValues[1]);
 		Serial.print("Right 45\t");
 		Serial.println(sensorValues[3]);
 		Serial.print("Right wall\t");
 		Serial.println(sensorValues[4]);

 }



void readSensors(){
	int sampleSize = 4;

	for(int a = 0; a < 5; a++){
		Average <int> measurements (sampleSize);
		for(int i = 0; i < sampleSize; i++){
			measurements.push(analogRead(irInput[a]));
		}
		sensorValues[a] = measurements.mean();
	}
}